<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Lightning Network>><label|c2 lightning-network>

  <subsection|Background><label|c2 lightning-network-bg>

  <subsection|History>

  <\itemize>
    <item>On 2017-01-04, the first snapshot of the <name|lnd> git repository
    appeared on the Internet Archive.<\footnote>
      See <hlink|<verbatim|https://web.archive.org/web/20170104082532/https://github.com/LightningNetwork/lnd>|https://web.archive.org/web/20170104082532/https://github.com/LightningNetwork/lnd>
    </footnote>
  </itemize>

  <subsection|Public Key Details><label|c2 lightning-network-pk>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|9>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|9>>
    <associate|auto-2|<tuple|1.1|9>>
    <associate|auto-3|<tuple|1.2|9>>
    <associate|auto-4|<tuple|1.3|9>>
    <associate|c2 lightning-network|<tuple|1|9>>
    <associate|c2 lightning-network-bg|<tuple|1.1|9>>
    <associate|c2 lightning-network-pk|<tuple|1.3|9>>
    <associate|footnote-1.1|<tuple|1.1|9>>
    <associate|footnr-1.1|<tuple|1.1|9>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      1<space|2spc><with|font-shape|<quote|small-caps>|Lightning Network>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>