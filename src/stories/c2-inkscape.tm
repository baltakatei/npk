<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Inkscape>><label|c2 inkscape>

  Last updated on 2023-06-11 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 inkscape-bg>

  <index-complex|<tuple|Software|Inkscape>|strong|c2 inkscape
  idx1|<tuple|Software|Inkscape>><name|Inkscape<\footnote>
    Main website: <hlinkv|https://inkscape.org>.
  </footnote>> is a vector graphics program developed by various
  individuals<\footnote>
    See <hlinkv|https://inkscape.org/*developer/>.
  </footnote> under a GPL license.

  <subsection|History><label|c2 inkscape-hist>

  <\description>
    <item*|2003-11-08>First <name|I.A.> snapshot of
    <hlinkv|https://www.inkscape.org>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20031108064840/http://www.inkscape.org/>.
    </footnote>

    <item*|2014-08-20>Early mention of <name|Marc Jeanmougin>'s PGP key
    fingerprint (<long-id-spc|5FCB 204E F882 B07A>).<\footnote>
      See <hlinkv|https://web.archive.org/web/20140820132900/http://marc.jeanmougin.fr/>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 inkscape-pk>

  <subsubsection|Signing key ( v0.92\Uv1.2) (<long-id-spc|5FCB 204E F882
  B07A>)>

  <index-complex|<tuple|Keys|People|Jeanmougin,
  Marc|0x5FCB204EF882B07A>|||<tuple|Keys|People|Jeanmougin,
  Marc|<verb-sm|0x5FCB204EF882B07A>>>Key used by <person|Marc
  Jeanmougin><\footnote>
    <name|Inkscape> developer. See <hlinkv|https://inkscape.org/~MarcJeanmougin/>.
  </footnote> to sign releases of <name|Inkscape> versions
  <strong|v0.92\Uv1.2>.

  <\vpk>
    pub \ \ dsa1024/0x5FCB204EF882B07A 2010-03-08 [SCA] [expired: 2023-04-09]

    \ \ \ \ \ \ Key fingerprint = 74E8 DA13 9805 5A81 20B2 \ 76EB 5FCB 204E
    F882 B07A

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Marc Jeanmougin
    \<less\>e4666e42\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Marc Jeanmougin
    \<less\>a787aba5\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Marc Jeanmougin
    \<less\>81fd8643\<gtr\>
  </vpk>

  <index-complex|<tuple|Software|Inkscape>|strong|c2 inkscape
  idx1|<tuple|Software|Inkscape>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|31>
    <associate|page-medium|paper>
    <associate|section-nr|8>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|9|31>>
    <associate|auto-2|<tuple|9.1|31>>
    <associate|auto-3|<tuple|<tuple|Software|Inkscape>|31>>
    <associate|auto-4|<tuple|9.2|31>>
    <associate|auto-5|<tuple|9.3|31>>
    <associate|auto-6|<tuple|9.3.1|31>>
    <associate|auto-7|<tuple|<tuple|Keys|People|Jeanmougin,
    Marc|0x5FCB204EF882B07A>|31>>
    <associate|auto-8|<tuple|<tuple|Software|Inkscape>|31>>
    <associate|c2 inkscape|<tuple|9|31>>
    <associate|c2 inkscape-bg|<tuple|9.1|31>>
    <associate|c2 inkscape-hist|<tuple|9.2|31>>
    <associate|c2 inkscape-pk|<tuple|9.3|31>>
    <associate|footnote-9.1|<tuple|9.1|31>>
    <associate|footnote-9.2|<tuple|9.2|31>>
    <associate|footnote-9.3|<tuple|9.3|31>>
    <associate|footnote-9.4|<tuple|9.4|31>>
    <associate|footnote-9.5|<tuple|9.5|31>>
    <associate|footnr-9.1|<tuple|9.1|31>>
    <associate|footnr-9.2|<tuple|9.2|31>>
    <associate|footnr-9.3|<tuple|9.3|31>>
    <associate|footnr-9.4|<tuple|9.4|31>>
    <associate|footnr-9.5|<tuple|9.5|31>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Inkscape>|strong|c2 inkscape
      idx1|<tuple|Software|Inkscape>|<pageref|auto-3>>

      <tuple|<tuple|Keys|People|Jeanmougin,
      Marc|0x5FCB204EF882B07A>|||<tuple|Keys|People|Jeanmougin,
      Marc|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x5FCB204EF882B07A>>>|<pageref|auto-7>>

      <tuple|<tuple|Software|Inkscape>|strong|c2 inkscape
      idx1|<tuple|Software|Inkscape>|<pageref|auto-8>>
    </associate>
    <\associate|toc>
      9<space|2spc><with|font-shape|<quote|small-caps>|Inkscape>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|9.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|9.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|9.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|2tab>|9.3.1<space|2spc>Signing key ( v0.92\Uv1.2)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|5FCB
      204E F882 B07A>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>
    </associate>
  </collection>
</auxiliary>