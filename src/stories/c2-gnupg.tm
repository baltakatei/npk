<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|GnuPG>><label|c2 gnupg>

  Last updated on 2023-06-15 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 gnupg-bg>

  <index-complex|<tuple|Software|GnuPG>|strong|c2 gnupg
  idx1|<tuple|Software|GnuPG>><subindex|Software|App
  Name><name|GnuPG<\footnote>
    Main website: <hlinkv|https://gnupg.org>.
  </footnote>> is a set of privacy-enhancing programs licensed<\footnote>
    See <hlinkv|https://web.archive.org/web/20070708182544/http:/lists.gnupg.org/pipermail/gnupg-announce/2007q3/000255.html>.
  </footnote> under the <name|GNU General Public License><\footnote>
    See <hlinkv|https://www.gnu.org/licenses/gpl-3.0.en.html>.
  </footnote> designed to encrypt and digitally sign data using the
  <subindex|Software|OpenPGP><name|OpenPGP> standard<\footnote>
    See RFC4880: <hlinkv|https://www.ietf.org/rfc/rfc4880.txt>.
  </footnote>. The name \P<name|GnuPG>\Q, or \P<name|GPG>\Q as its main
  program <verbatim|gpg> is called, is a play on words alluding to the
  acronym \P<name|PGP>\Q. <name|PGP>, which stands for \PPretty Good
  Privacy\Q was a commercial program created by <subindex|People|Zimmermann,
  Phil><name|Phil Zimmermann> in 1991 from which the <name|OpenPGP> standard
  was derived.

  Compiled binary releases of <name|GnuPG> themselves use <name|GnuPG> to
  verify their own integrity. A list of public keys used to sign releases is
  provided in Table <reference|c2 tab gnupg signing keys>. If a trusted
  instance of <name|GnuPG> is not available, checksums of releases are
  published on the <hlinkv|https://gnupg.org> website.<\footnote>
    See <hlinkv|https://gnupg.org/download/integrity_check.html>.
  </footnote> That said, <name|GnuPG> is often installed by default on
  <name|Gnu/Linux> operating systems, such as <name|Debian> (see
  <reference|c2 debian>), in which software package managers, such as
  <name|APT>, automatically use <name|GnuPG> to verify the integrity of
  downloaded software<\footnote>
    See <hlinkv|https://wiki.debian.org/SecureApt>.
  </footnote>. In particular, <name|Debian> also verifies developer
  identities using <name|OpenPGP> public keys<\footnote>
    See <hlinkv|https://www.debian.org/devel/join/nm-step2>.
  </footnote>.

  As of 2023, the <name|GnuPG> project is maintained by
  <subindex|People|Koch, Werner><name|Werner Koch> (<long-id-spc|5288 97B8
  2640 3ADA>) and funded by commercial support contracts for a version of
  <name|gpg4win> called \PGnuPG VS-Desktop\Q.<\footnote>
    See <hlinkv|https://gnupg.org/blog/20220102-a-new-future-for-gnupg.html>.
  </footnote> Prior to 2022, a significant fraction of project funding
  originated from donations by individuals.<\footnote>
    See <hlinkv|https://gnupg.org/donate/kudos.html>.
  </footnote>

  <subsection|History><label|c2 gnupg-hist>

  <\description-compact>
    <item*|1998-07-07><name|Werner Koch> creates first release signing key
    <long-id-spc|68B7 AB89 5754 8DCD>.

    <item*|1999-01-29>Date of early webpage I.A. snapshot at
    <hlinkv|http://www.k.shuttle.de>domain.<\footnote>
      See <hlinkv|https://web.archive.org/web/20000303105255/http://www.gnupg.org/>.
    </footnote>

    <item*|1999-09-07><name|GnuPG> version <verbatim|1.0.0>
    released.<\footnote>
      See <hlinkv|https://web.archive.org/web/20040318173823/http://lists.gnupg.org/pipermail/gnupg-announce/1999q3/000037.html>.
    </footnote>

    <item*|2000-03-03>Date of early I.A. snapshot of
    <hlinkv|http://www.gnupg.org>domain.<\footnote>
      See <hlinkv|https://web.archive.org/web/20000303105255/http://www.gnupg.org/>.
    </footnote>

    <item*|2001-05-03>Date of early I.A. snapshot of
    <hlinkv|https://www.g10.code.com> domain.<\footnote>
      See <hlinkv|https://web.archive.org/web/20010503130044/http://www.g10code.com/>.
    </footnote>\ 

    <item*|2006-11-11><name|GnuPG> version <verbatim|2.0.0>
    released.<\footnote>
      See <hlinkv|https://web.archive.org/web/20061117172350/http://lists.gnupg.org/pipermail/gnupg-announce/2006q4/000239.html>.
    </footnote>
  </description-compact>

  <subsection|Public Key Details><label|c2 gnupg-pk>

  Various public keys have been used to sign compiled binary releases of
  <name|GnuPG>.<float|float|hb|<\big-table|<tabular|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|||||<cwith|-1|-1|1|-1|cell-bborder|1ln>|||<table|<row|<cell|Cr.
  Date>|<cell|Long ID>|<cell|UID>|<cell|Used>|<cell|Link>>|<row|<cell|1998-07-07>|<cell|<long-id-spc|68B7
  AB89 5754 8DCD>>|<cell|<verb-sm|Werner Koch (gnupg sig)>>|<cell|1998\U2005
  <note-ref|+6MbRfX9zTo5YKs>>|<cell|<note-ref|+6MbRfX9zTo5YKr>>>|<row|<cell|2006-01-01>|<cell|<long-id-spc|53B6
  20D0 1CE0 C630>>|<cell|<verb-sm|Werner Koch (dist sig)>>|<cell|1996\U2010
  <note-ref|+6MbRfX9zTo5YKs>>|<cell|<note-ref|+6MbRfX9zTo5YKt>>>|<row|<cell|2011-01-12>|<cell|<long-id-spc|249B
  39D2 4F25 E3B6>>|<cell|<verb-sm|Werner Koch (dist
  sig)>>|<cell|2011\U2021>|<cell|<note-ref|+6MbRfX9zTo5YKt>>>|<row|<cell|2014-10-29>|<cell|<long-id-spc|0437
  6F3E E085 6959>>|<cell|<verb-sm|David Shaw (GnuPG Release Signing
  Key)>>|<cell|2015\U2020>|<cell|<note-ref|+6MbRfX9zTo5YKu>>>|<row|<cell|2014-10-29>|<cell|<long-id-spc|2071
  B08A 33BD 3F06>>|<cell|<verb-sm|NIIBE Yutaka (GnuPG Release
  Key)>>|<cell|2015\U2021>|<cell|<note-ref|+6MbRfX9zTo5YKu>>>|<row|<cell|2014-10-19>|<cell|<long-id-spc|8A86
  1B1C 7EFD 60D9>>|<cell|<verb-sm|Werner Koch (Release Signing
  Key)>>|<cell|2015\U2017>|<cell|<note-ref|+6MbRfX9zTo5YKu>>>|<row|<cell|2017-03-17>|<cell|<long-id-spc|BCEF
  7E29 4B09 2E28>>|<cell|<verb-sm|Andre Heinecke (Release Signing
  Key)>>|<cell|2017\U>|<cell|<note-ref|+6MbRfX9zTo5YKv>>>|<row|<cell|2020-08-24>|<cell|<long-id-spc|5288
  97B8 2640 3ADA>>|<cell|<verb-sm|Werner Koch (dist signing
  2020)>>|<cell|2020\U>|<cell|<note-ref|+6MbRfX9zTo5YKw>>>|<row|<cell|2021-05-19>|<cell|<long-id-spc|E98E
  9B2D 19C6 C8BD>>|<cell|<verb-sm|Niibe Yutaka (GnuPG Release
  Key)>>|<cell|2021\U>|<cell|<note-ref|+6MbRfX9zTo5YKx>>>|<row|<cell|2021-10-15>|<cell|<long-id-spc|549E
  695E 905B A208>>|<cell|<verb-sm|GnuPG.com (Release Signing Key
  2021)>>|<cell|2021\U>|<cell|<note-ref|+6MbRfX9zTo5YKy>>>>>>>
    <label|c2 tab gnupg signing keys>A list of keys used to sign <name|GnuPG>
    releases. Keys identified from <name|Internet Archive> snapshots of
    \ <hlinkv|http://www.gnupg.org/signature_key.html>.

    <note-inline|Date span source: <hlinkv|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>.|+6MbRfX9zTo5YKs>

    <note-inline|See <hlinkv|https://web.archive.org/web/20041113170551/http://www.gnupg.org/signature_key.html>.|+6MbRfX9zTo5YKr>

    <note-inline|See <hlinkv|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>.|+6MbRfX9zTo5YKt>

    <note-inline|See <hlinkv|https://web.archive.org/web/20150503220844/https://www.gnupg.org/signature_key.html>.|+6MbRfX9zTo5YKu>

    <note-inline|See <hlinkv|https://web.archive.org/web/20180515231121/https://gnupg.org/signature_key.html>.|+6MbRfX9zTo5YKv>

    <note-inline|See <hlinkv|https://web.archive.org/web/20200917215036/https://gnupg.org/signature_key.html>.|+6MbRfX9zTo5YKw>

    <note-inline|See <hlinkv|https://web.archive.org/web/20210923054234/https://www.gnupg.org/signature_key.html>.|+6MbRfX9zTo5YKx>

    <note-inline|See <hlinkv|https://web.archive.org/web/20211018075758/https://gnupg.org/signature_key.html>.|+6MbRfX9zTo5YKy>

    \;

    \;
  </big-table>> Below are the keys valid as of 2022.

  <subsubsection|Release signing key - <name|Werner Koch> (2020\U)
  (<long-id-spc|5288 97B8 2640 3ADA>)><label|c2 gnupg-pk-werner-3ada>

  <index-complex|<tuple|Keys|People|Koch,
  Werner|0x528897B826403ADA>|||<tuple|Keys|People|Koch,
  Werner|<verb-sm|0x528897B826403ADA>>>Key used by <name|Werner Koch> to sign
  releases of <name|GnuPG> since 2020.

  <\vpk>
    pub \ \ ed25519/0x528897B826403ADA 2020-08-24 [SC] [expires: 2030-06-30]

    \ \ \ \ \ \ Key fingerprint = 6DAA 6E64 A76D 2840 571B \ 4902 5288 97B8
    2640 3ADA

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Werner Koch (dist
    signing 2020)
  </vpk>

  <subsubsection|Release signing key - <name|Niibe Yutaka> (2021\U)
  (<long-id-spc|E98E 9B2D 19C6 C8BD>)><label|c2 gnupg-pk-niibe-c8bd>

  <index-complex|<tuple|Keys|People|Yutaka,
  Niibe|0xE98E9B2D19C6C8BD>|||<tuple|Keys|People|Yutaka,
  Niibe|<verb-sm|0xE98E9B2D19C6C8BD>>>Key used by <name|Niibe Yutaka> to sign
  releases of <name|GnuPG> since 2021.

  <\vpk>
    pub \ \ ed25519/0xE98E9B2D19C6C8BD 2021-05-19 [SC] [expires: 2027-04-04]

    \ \ \ \ \ \ Key fingerprint = AC8E 115B F73E 2D8D 47FA \ 9908 E98E 9B2D
    19C6 C8BD

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Niibe Yutaka (GnuPG
    Release Key)
  </vpk>

  <subsubsection|Release signing key - <name|Andre Heinecke> (2017\U)
  (<long-id-spc|BCEF 7E29 4B09 2E28>)><label|c2 gnupg-pk-andre-c8bd>

  <index-complex|<tuple|Keys|People|Heinecke,
  Andre|0xBCEF7E294B092E28>|||<tuple|Keys|People|Heinecke,
  Andre|<verb-sm|0xBCEF7E294B092E28>>>Key used by <name|Andre Heinecke> to
  sign releases of <name|GnuPG> since 2017.

  <\vpk>
    pub \ \ rsa3072/0xBCEF7E294B092E28 2017-03-17 [SC] [expires: 2027-03-15]

    \ \ \ \ \ \ Key fingerprint = 5B80 C575 4298 F0CB 55D8 \ ED6A BCEF 7E29
    4B09 2E28

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Andre Heinecke
    (Release Signing Key)
  </vpk>

  <subsubsection|Release signing key - <name|GnuPG.com> (2021\U)
  (<long-id-spc|549E 695E 905B A208>)><label|c2 gnupg-pk-gnupgcom-a208>

  <index-complex|<tuple|Keys|Organizations|GnuPG.com|0x549E695E905BA208>|||<tuple|Keys|Organizations|GnuPG.com|<verb-sm|0x549E695E905BA208>>>Key
  used by the <name|g10 Code GmbH> organization under the <name|GnuPG.com>
  brand to sign releases of <name|GnuPG> since 2021.<\footnote>
    See <hlinkv|https://gnupg.org/blog/20220102-a-new-future-for-gnupg.html>for
    origin of <name|GnuPG.com> brand.
  </footnote>

  <\vpk>
    pub \ \ brainpoolP256r1/0x549E695E905BA208 2021-10-15 [SC] [expires:
    2029-12-31]

    \ \ \ \ \ \ Key fingerprint = 02F3 8DFF 731F F97C B039 \ A1DA 549E 695E
    905B A208

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] GnuPG.com (Release
    Signing Key 2021)

    sub \ \ brainpoolP256r1/0x9CDA5DC48371F0E3 2021-10-15 [A] [expires:
    2029-12-31]

    \ \ \ \ \ \ Key fingerprint = 6819 7595 44AC 985D 8D52 \ 6066 9CDA 5DC4
    8371 F0E3
  </vpk>

  <index-complex|<tuple|Software|GnuPG>|strong|c2 gnupg
  idx1|<tuple|Software|GnuPG>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|30>
    <associate|page-medium|papyrus>
    <associate|preamble|false>
    <associate|section-nr|8>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|9|30>>
    <associate|auto-10|<tuple|9.22|31>>
    <associate|auto-11|<tuple|9.3.1|31>>
    <associate|auto-12|<tuple|<tuple|Keys|People|Koch,
    Werner|0x528897B826403ADA>|31>>
    <associate|auto-13|<tuple|9.3.2|31>>
    <associate|auto-14|<tuple|<tuple|Keys|People|Yutaka,
    Niibe|0xE98E9B2D19C6C8BD>|31>>
    <associate|auto-15|<tuple|9.3.3|31>>
    <associate|auto-16|<tuple|<tuple|Keys|People|Heinecke,
    Andre|0xBCEF7E294B092E28>|31>>
    <associate|auto-17|<tuple|9.3.4|31>>
    <associate|auto-18|<tuple|<tuple|Keys|Organizations|GnuPG.com|0x549E695E905BA208>|31>>
    <associate|auto-19|<tuple|<tuple|Software|GnuPG>|31>>
    <associate|auto-2|<tuple|9.1|30>>
    <associate|auto-3|<tuple|<tuple|Software|GnuPG>|30>>
    <associate|auto-4|<tuple|Software|30>>
    <associate|auto-5|<tuple|Software|30>>
    <associate|auto-6|<tuple|People|30>>
    <associate|auto-7|<tuple|People|30>>
    <associate|auto-8|<tuple|9.2|30>>
    <associate|auto-9|<tuple|9.3|31>>
    <associate|c2 gnupg|<tuple|9|30>>
    <associate|c2 gnupg-bg|<tuple|9.1|30>>
    <associate|c2 gnupg-hist|<tuple|9.2|30>>
    <associate|c2 gnupg-pk|<tuple|9.3|31>>
    <associate|c2 gnupg-pk-andre-c8bd|<tuple|9.3.3|31>>
    <associate|c2 gnupg-pk-gnupgcom-a208|<tuple|9.3.4|31>>
    <associate|c2 gnupg-pk-niibe-c8bd|<tuple|9.3.2|31>>
    <associate|c2 gnupg-pk-werner-3ada|<tuple|9.3.1|31>>
    <associate|c2 tab gnupg signing keys|<tuple|9.1|31>>
    <associate|footnote-9.1|<tuple|9.1|30>>
    <associate|footnote-9.10|<tuple|9.10|30>>
    <associate|footnote-9.11|<tuple|9.11|30>>
    <associate|footnote-9.12|<tuple|9.12|30>>
    <associate|footnote-9.13|<tuple|9.13|30>>
    <associate|footnote-9.14|<tuple|9.14|30>>
    <associate|footnote-9.15|<tuple|9.15|31>>
    <associate|footnote-9.16|<tuple|9.16|31>>
    <associate|footnote-9.17|<tuple|9.17|31>>
    <associate|footnote-9.18|<tuple|9.18|31>>
    <associate|footnote-9.19|<tuple|9.19|31>>
    <associate|footnote-9.2|<tuple|9.2|30>>
    <associate|footnote-9.20|<tuple|9.20|31>>
    <associate|footnote-9.21|<tuple|9.21|31>>
    <associate|footnote-9.22|<tuple|9.22|31>>
    <associate|footnote-9.23|<tuple|9.23|31>>
    <associate|footnote-9.3|<tuple|9.3|30>>
    <associate|footnote-9.4|<tuple|9.4|30>>
    <associate|footnote-9.5|<tuple|9.5|30>>
    <associate|footnote-9.6|<tuple|9.6|30>>
    <associate|footnote-9.7|<tuple|9.7|30>>
    <associate|footnote-9.8|<tuple|9.8|30>>
    <associate|footnote-9.9|<tuple|9.9|30>>
    <associate|footnr-9.1|<tuple|9.1|30>>
    <associate|footnr-9.10|<tuple|9.10|30>>
    <associate|footnr-9.11|<tuple|9.11|30>>
    <associate|footnr-9.12|<tuple|9.12|30>>
    <associate|footnr-9.13|<tuple|9.13|30>>
    <associate|footnr-9.14|<tuple|9.14|30>>
    <associate|footnr-9.15|<tuple|9.16|31>>
    <associate|footnr-9.16|<tuple|9.16|31>>
    <associate|footnr-9.17|<tuple|9.17|31>>
    <associate|footnr-9.18|<tuple|9.18|31>>
    <associate|footnr-9.19|<tuple|9.19|31>>
    <associate|footnr-9.2|<tuple|9.2|30>>
    <associate|footnr-9.20|<tuple|9.20|31>>
    <associate|footnr-9.21|<tuple|9.21|31>>
    <associate|footnr-9.22|<tuple|9.22|31>>
    <associate|footnr-9.23|<tuple|9.23|31>>
    <associate|footnr-9.3|<tuple|9.3|30>>
    <associate|footnr-9.4|<tuple|9.4|30>>
    <associate|footnr-9.5|<tuple|9.5|30>>
    <associate|footnr-9.6|<tuple|9.6|30>>
    <associate|footnr-9.7|<tuple|9.7|30>>
    <associate|footnr-9.8|<tuple|9.8|30>>
    <associate|footnr-9.9|<tuple|9.9|30>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|GnuPG>|strong|c2 gnupg
      idx1|<tuple|Software|GnuPG>|<pageref|auto-3>>

      <tuple|<tuple|Software|App Name>|<pageref|auto-4>>

      <tuple|<tuple|Software|OpenPGP>|<pageref|auto-5>>

      <tuple|<tuple|People|Zimmermann, Phil>|<pageref|auto-6>>

      <tuple|<tuple|People|Koch, Werner>|<pageref|auto-7>>

      <tuple|<tuple|Keys|People|Koch, Werner|0x528897B826403ADA>|||<tuple|Keys|People|Koch,
      Werner|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x528897B826403ADA>>>|<pageref|auto-12>>

      <tuple|<tuple|Keys|People|Yutaka, Niibe|0xE98E9B2D19C6C8BD>|||<tuple|Keys|People|Yutaka,
      Niibe|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE98E9B2D19C6C8BD>>>|<pageref|auto-14>>

      <tuple|<tuple|Keys|People|Heinecke,
      Andre|0xBCEF7E294B092E28>|||<tuple|Keys|People|Heinecke,
      Andre|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xBCEF7E294B092E28>>>|<pageref|auto-16>>

      <tuple|<tuple|Keys|Organizations|GnuPG.com|0x549E695E905BA208>|||<tuple|Keys|Organizations|GnuPG.com|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x549E695E905BA208>>>|<pageref|auto-18>>

      <tuple|<tuple|Software|GnuPG>|strong|c2 gnupg
      idx1|<tuple|Software|GnuPG>|<pageref|auto-19>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.1>|>
        A list of keys used to sign <with|font-shape|<quote|small-caps>|GnuPG>
        releases. Keys identified from <with|font-shape|<quote|small-caps>|Internet
        Archive> snapshots of \ <locus|<id|%375DDA8-4005580>|<link|hyperlink|<id|%375DDA8-4005580>|<url|http://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|http://www.gnupg.org/signature_key.html>>
        >.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-415BB00>|<link|hyperlink|<id|%375DDA8-415BB00>|<url|#footnr-9.15>>|9.15>.
        |<hidden-binding|<tuple|footnote-9.15>|9.15>|Date span source:
        <locus|<id|%375DDA8-415C240>|<link|hyperlink|<id|%375DDA8-415C240>|<url|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-415C410>|<link|hyperlink|<id|%375DDA8-415C410>|<url|#footnr-9.16>>|9.16>.
        |<hidden-binding|<tuple|footnote-9.16>|9.16>|See
        <locus|<id|%375DDA8-415BB48>|<link|hyperlink|<id|%375DDA8-415BB48>|<url|https://web.archive.org/web/20041113170551/http://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20041113170551/http://www.gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-415C5B0>|<link|hyperlink|<id|%375DDA8-415C5B0>|<url|#footnr-9.17>>|9.17>.
        |<hidden-binding|<tuple|footnote-9.17>|9.17>|See
        <locus|<id|%375DDA8-415BD20>|<link|hyperlink|<id|%375DDA8-415BD20>|<url|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-415B4E8>|<link|hyperlink|<id|%375DDA8-415B4E8>|<url|#footnr-9.18>>|9.18>.
        |<hidden-binding|<tuple|footnote-9.18>|9.18>|See
        <locus|<id|%375DDA8-415C348>|<link|hyperlink|<id|%375DDA8-415C348>|<url|https://web.archive.org/web/20150503220844/https://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20150503220844/https://www.gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-415C5F8>|<link|hyperlink|<id|%375DDA8-415C5F8>|<url|#footnr-9.19>>|9.19>.
        |<hidden-binding|<tuple|footnote-9.19>|9.19>|See
        <locus|<id|%375DDA8-415CAC8>|<link|hyperlink|<id|%375DDA8-415CAC8>|<url|https://web.archive.org/web/20180515231121/https://gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20180515231121/https://gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-415AA48>|<link|hyperlink|<id|%375DDA8-415AA48>|<url|#footnr-9.20>>|9.20>.
        |<hidden-binding|<tuple|footnote-9.20>|9.20>|See
        <locus|<id|%375DDA8-41548D0>|<link|hyperlink|<id|%375DDA8-41548D0>|<url|https://web.archive.org/web/20200917215036/https://gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20200917215036/https://gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-4154A60>|<link|hyperlink|<id|%375DDA8-4154A60>|<url|#footnr-9.21>>|9.21>.
        |<hidden-binding|<tuple|footnote-9.21>|9.21>|See
        <locus|<id|%375DDA8-41549F0>|<link|hyperlink|<id|%375DDA8-41549F0>|<url|https://web.archive.org/web/20210923054234/https://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20210923054234/https://www.gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%375DDA8-4154DD8>|<link|hyperlink|<id|%375DDA8-4154DD8>|<url|#footnr-9.22>>|9.22>.
        |<hidden-binding|<tuple|footnote-9.22>|9.22>|See
        <locus|<id|%375DDA8-4155F18>|<link|hyperlink|<id|%375DDA8-4155F18>|<url|https://web.archive.org/web/20211018075758/https://gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20211018075758/https://gnupg.org/signature_key.html>>
        >.>>>

        \;

        \;
      </surround>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      9<space|2spc><with|font-shape|<quote|small-caps>|GnuPG>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|9.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|9.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|9.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|9.3.1<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|Werner Koch> (2020\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|5288
      97B8 2640 3ADA>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|2tab>|9.3.2<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|Niibe Yutaka> (2021\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E98E
      9B2D 19C6 C8BD>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|9.3.3<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|Andre Heinecke> (2017\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|BCEF
      7E29 4B09 2E28>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|2tab>|9.3.4<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|GnuPG.com> (2021\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|549E
      695E 905B A208>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>
    </associate>
  </collection>
</auxiliary>