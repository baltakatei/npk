<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|VeraCrypt>><label|c2 veracrypt>

  Last updated on 2023-06-12 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 veracrypt-bg>

  <index-complex|<tuple|Software|Veracrypt>|strong|c2 veracrypt
  idx1|<tuple|Software|Veracrypt>><name|VeraCrypt<\footnote>
    Main website: <hlinkv|https://veracrypt.fr>.
  </footnote>> is an encryption software package compatible with
  <name|Windows>, <name|macOS>, and <name|GNU/Linux> operating systems. The
  program is primarily maintained by <subindex|People|Idrassi,
  Mounir><name|Mounir Idrassi>.

  <name|VeraCrypt> is a fork of <name|TrueCrypt> made in 2013.

  <subsection|History><label|c2 veracrypt-hist>

  <\description>
    <item*|2013-06-29>First <name|I.A.> snapshot of
    <hlinkv|http://veracrypt.codeplex.com>, <name|VeraCrypt>'s first public
    repository address.

    <item*|2014-07-15>Early mention of full <long-id-spc|EB55 9C7C 54DD D393>
    fingerprint on <hlinkv|www.idrix.fr>website.<\footnote>
      See <hlinkv|https://web.archive.org/web/20140715152305/http://www.idrix.fr:80/Root/content/category/7/32/60>.
    </footnote>

    <item*|2014-05-28><name|TrueCrypt> development halt announcement posted
    on <name|SourceForge> repository.<\footnote>
      Goodin, Dan. \P'Truecrypt is not secure,' official SourceForge page
      abruptly warns\Q. Date: 2014-05-28. URL:
      <hlinkv|https://arstechnica.com/information-technology/2014/05/truecrypt-is-not-secure-official-sourceforge-page-abruptly-warns/>.
      Access date: 2022-03-12. Archive URL:
      <hlinkv|https://web.archive.org/web/20140529084822/http://arstechnica.com/security/2014/05/truecrypt-is-not-secure-official-sourceforge-page-abruptly-warns/>.
      Archive date: 2014-05-29.
    </footnote>

    <item*|2014-06-27>Creation date of signing key <long-id-spc|EB55 9C7C
    54DD D393>.

    <item*|2016-10-17><name|VeraCrypt> <em|v1.18> audited by
    <subindex|Organizations|QuarksLab><name|QuarksLab> and <em|v1.19>
    released to fix most reported vulnerabilities.<\footnote>
      ostifadmin. \PThe Veracrypt Audit Results\Q. Website:
      <hlinkv|ostif.org>. Date: 2016-10-17. URL:
      <hlinkv|https://ostif.org/the-veracrypt-audit-results/>. Archive URL:
      <hlinkv|https://web.archive.org/web/20161017182455/https://ostif.org/the-veracrypt-audit-results/>.
      Archive date: 2016-10-17.
    </footnote>.

    <item*|2017-05-25>First <name|I.A.> snapshot of
    <hlinkv|https://veracrypt.fr><\footnote>
      See <hlinkv|https://web.archive.org/web/20170525235647/https://www.veracrypt.fr/en/Home.html>.
    </footnote>, the new website made in response to <name|Microsoft>
    shutting down <hlinkv|codeplex.com>in 2017<\footnote>
      Harry, Brian. \PShutting down CodePlex\Q. Website:
      <hlinkv|devblogs.microsoft.com>. Date: 2017-03-31. Access date:
      2022-03-12. \ <hlinkv|https://devblogs.microsoft.com/bharry/shutting-down-codeplex/>.
    </footnote>.

    <item*|2018-09-11>Creation date of signing key <long-id-spc|821A CD02
    680D 16DE>.

    <item*|2018-09-12>Signing key <long-id-spc|EB55 9C7C 54DD D393> retired
    and replaced by key <long-id-spc|821A CD02 680D 16DE> via a transition
    statement signed by both keys.<\footnote>
      See <hlinkv|https://web.archive.org/web/20181223051800/https://veracrypt.fr/pgp-key-transition-2018-09-12.txt>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 veracrypt-pk>

  <subsubsection|Signing key (2018\U ) (<long-id-spc|821A CD02 680D 16DE>)>

  <index-complex|<tuple|Keys|Organizations|Veracrypt|Signing|0x821ACD02680D16DE
  (2018\U )>|||<tuple|Keys|Organizations|Veracrypt|Signing|<verb-sm|0x821ACD02680D16DE>
  (2018\U )>>Key used to sign <name|VeraCrypt> releases since version
  <em|v1.23> in 2018. A copy of this key can be downloaded here<\footnote>
    See <hlinkv|https://web.archive.org/web/20220211073947/https://www.idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key.asc>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0x821ACD02680D16DE 2018-09-11 [SC]

    \ \ \ \ \ \ Key fingerprint = 5069 A233 D55A 0EEB 174A \ 5FC3 821A CD02
    680D 16DE

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] VeraCrypt Team (2018 -
    Supersedes Key ID=0x54DDD393) \<less\>8042d942\<gtr\>

    sub \ \ rsa4096/0x200B5A9D26878A32 2018-09-11 [E]

    \ \ \ \ \ \ Key fingerprint = BB33 5DCA 0D75 325C 6126 \ BAB6 200B 5A9D
    2687 8A32

    sub \ \ rsa4096/0x0F5AACD65483D029 2018-09-11 [A]

    \ \ \ \ \ \ Key fingerprint = 6022 69E6 D482 C250 0D1C \ 2D87 0F5A ACD6
    5483 D029
  </vpk>

  <subsubsection|Signing key (2014\U2018) (<long-id-spc|EB55 9C7C 54DD
  D393>)>

  <index-complex|<tuple|Keys|Organizations|Veracrypt|Signing|0xEB559C7C54DDD393
  (2014\U2018)>|||<tuple|Keys|Organizations|Veracrypt|Signing|<verb-sm|0xEB559C7C54DDD393>
  (2014\U2018)>>Key used to sign <name|VeraCrypt> releases prior to version
  <em|v1.23> in 2018. This key was used to sign the version of
  <name|VeraCrypt> audited by <subindex|Organizations|QuarksLab><name|QuarksLab>
  in 2016 (<em|v1.18>). A 2014 copy of this public key is available
  here<\footnote>
    See <hlinkv|https://web.archive.org/web/20200307044514/https://www.idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key_2014.asc>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0xEB559C7C54DDD393 2014-06-27 [SCE]

    \ \ \ \ \ \ Key fingerprint = 993B 7D7E 8E41 3809 828F \ 0F29 EB55 9C7C
    54DD D393

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] VeraCrypt Team
    \<less\>8042d942\<gtr\>
  </vpk>

  <index-complex|<tuple|Software|Veracrypt>|strong|c2 veracrypt
  idx1|<tuple|Software|Veracrypt>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|41>
    <associate|page-medium|papyrus>
    <associate|section-nr|15>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|16|31>>
    <associate|auto-10|<tuple|16.3.2|32>>
    <associate|auto-11|<tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0xEB559C7C54DDD393
    (2014\U2018)>|32>>
    <associate|auto-12|<tuple|Organizations|32>>
    <associate|auto-13|<tuple|<tuple|Software|Veracrypt>|32>>
    <associate|auto-2|<tuple|16.1|31>>
    <associate|auto-3|<tuple|<tuple|Software|Veracrypt>|31>>
    <associate|auto-4|<tuple|People|31>>
    <associate|auto-5|<tuple|16.2|31>>
    <associate|auto-6|<tuple|Organizations|31>>
    <associate|auto-7|<tuple|16.3|31>>
    <associate|auto-8|<tuple|16.3.1|31>>
    <associate|auto-9|<tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0x821ACD02680D16DE
    (2018\U )>|31>>
    <associate|c2 veracrypt|<tuple|16|31>>
    <associate|c2 veracrypt-bg|<tuple|16.1|31>>
    <associate|c2 veracrypt-hist|<tuple|16.2|31>>
    <associate|c2 veracrypt-pk|<tuple|16.3|31>>
    <associate|footnote-16.1|<tuple|16.1|?>>
    <associate|footnote-16.2|<tuple|16.2|?>>
    <associate|footnote-16.3|<tuple|16.3|?>>
    <associate|footnote-16.4|<tuple|16.4|?>>
    <associate|footnote-16.5|<tuple|16.5|?>>
    <associate|footnote-16.6|<tuple|16.6|?>>
    <associate|footnote-16.7|<tuple|16.7|?>>
    <associate|footnote-16.8|<tuple|16.8|?>>
    <associate|footnote-16.9|<tuple|16.9|?>>
    <associate|footnr-16.1|<tuple|16.1|?>>
    <associate|footnr-16.2|<tuple|16.2|?>>
    <associate|footnr-16.3|<tuple|16.3|?>>
    <associate|footnr-16.4|<tuple|16.4|?>>
    <associate|footnr-16.5|<tuple|16.5|?>>
    <associate|footnr-16.6|<tuple|16.6|?>>
    <associate|footnr-16.7|<tuple|16.7|?>>
    <associate|footnr-16.8|<tuple|16.8|?>>
    <associate|footnr-16.9|<tuple|16.9|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Veracrypt>|strong|c2 veracrypt
      idx1|<tuple|Software|Veracrypt>|<pageref|auto-3>>

      <tuple|<tuple|People|Idrassi, Mounir>|<pageref|auto-4>>

      <tuple|<tuple|Organizations|QuarksLab>|<pageref|auto-6>>

      <tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0x821ACD02680D16DE
      (2018\U )>|||<tuple|Keys|Organizations|Veracrypt|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x821ACD02680D16DE>>
      (2018\U )>|<pageref|auto-9>>

      <tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0xEB559C7C54DDD393
      (2014\U2018)>|||<tuple|Keys|Organizations|Veracrypt|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xEB559C7C54DDD393>>
      (2014\U2018)>|<pageref|auto-11>>

      <tuple|<tuple|Organizations|QuarksLab>|<pageref|auto-12>>

      <tuple|<tuple|Software|Veracrypt>|strong|c2 veracrypt
      idx1|<tuple|Software|Veracrypt>|<pageref|auto-13>>
    </associate>
    <\associate|toc>
      16<space|2spc><with|font-shape|<quote|small-caps>|VeraCrypt>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|16.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|16.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|16.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|2tab>|16.3.1<space|2spc>Signing key (2018\U )
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|821A
      CD02 680D 16DE>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|2tab>|16.3.2<space|2spc>Signing key (2014\U2018)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|EB55
      9C7C 54DD D393>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>
    </associate>
  </collection>
</auxiliary>