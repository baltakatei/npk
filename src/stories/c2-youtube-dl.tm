<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Youtube-dl>><label|c2 ytdl>

  Last updated 2023-06-24 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 ytdl-bg>

  <index-complex|<tuple|Software|Youtube-dl>|strong|c2 ytdl
  idx1|<tuple|Software|Youtube-dl>><name|Youtube-dl><\footnote>
    Main website: <hlinkv|https://yt-dl.org>.
  </footnote> is a <name|Python2>-based<\footnote>
    See <hlinkv|https://developers.slashdot.org/story/22/01/30/003205/youtube-dl-forks-to-continue-supporting-older-versions-of-python>.
  </footnote> program that can be used to download audio-visual media files
  from sites including, but not limited to, <name|Youtube>. The software
  gained notoreity in 2020 when <name|GitHub> took down the project page upon
  receiving a DMCA takedown notice issued by the RIAA.<\footnote>
    See <hlinkv|https://www.zdnet.com/article/riaa-blitz-takes-down-18-github-projects-used-for-downloading-youtube-videos/>.
  </footnote>

  As of 2021, the most project maintainer was <subindex|People|M.,
  Sergey><name|Sergey M.> (<long-id-spc|2C39 3E0F 18A9 236D>).

  As of 2023-06-15, no major release has been published since 2021. The
  defacto project successor in the <name|Debian> repository is <name|Yt-dlp>,
  a fork made by <name|pukkandan> (<long-id-spc|7EEE 9E1E 817D 0A39>).

  <subsection|History><label|c2 ytdl-hist>

  <\description>
    <item*|2008-07-21>First commit in the main project <name|Git> repository
    published by <subindex|People|Garcia, Ricardo><name|Ricardo
    Garcia>.<\footnote>
      See <hlinkv|https://github.com/ytdl-org/youtube-dl/commit/4fa74b5252a23c2890ddee52b8ee5811b5bb2987>.
    </footnote>

    <item*|2013-08-01>First image of the homepage <hlinkv|https://yt-dl.org>
    appears on the <name|Internet Archive>.

    <item*|2020-10-23><name|GitHub> project page taken down due to
    <name|DCMA> takedown notice<\footnote>
      See <hlinkv|https://github.com/github/dmca/blob/master/2020/10/2020-10-23-RIAA.md>.
    </footnote> issued by the <name|RIAA>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20201023194520/https://github.com/ytdl-org/youtube-dl>.
    </footnote>

    <item*|2020-11-16><name|GitHub> page for <name|Youtube-dl>
    reinstated.<\footnote>
      See <hlinkv|https://github.blog/2020-11-16-standing-up-for-developers-youtube-dl-is-back/>.
    </footnote>

    <item*|2021-12-25>The only active developer is <name|Remita Amine>
    (?).<\footnote>
      See <hlinkv|https://web.archive.org/web/20211225064545/https://ytdl-org.github.io/youtube-dl/about.html>.
    </footnote>

    <item*|2022-01-29>The project announced<\footnote>
      See <hlinkv|https://github.com/ytdl-org/youtube-dl/issues/30568>.
    </footnote> that it is seeking a new maintainer, that <name|Youtube-dl>
    would continue to support <name|Python2>, and that the fork
    <name|yt<nbhyph>dlp> created by <name|pukkandan> (<long-id-spc|7EEE 9E1E
    817D 0A39>) would support <name|Python3>.
  </description>

  <subsection|Public Key Details><label|c2 ytdl-pk>

  <subsubsection|Binary signing key. <name|Sergey M.> (<long-id-spc|2C39 3E0F
  18A9 236D>)>

  <index-complex|<tuple|Keys|People|M., Sergey|0x2C393E0F18A9236D>|||<tuple|Keys|People|D.,
  Sergey|<verb-sm|0x2C393E0F18A9236D>>>The binary signing key used to sign
  releases as of 2021. Owned by Sergey M.

  <\vpk>
    pub \ \ rsa4096/0x2C393E0F18A9236D 2016-04-09 [SC]

    \ \ \ \ \ \ Key fingerprint = ED7F 5BF4 6B3B BED8 1C87 \ 368E 2C39 3E0F
    18A9 236D

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Sergey M.
    \<less\>7345ddad\<gtr\>

    sub \ \ rsa4096/0xC3A4FE63297B1CE1 2016-04-09 [E]

    \ \ \ \ \ \ Key fingerprint = 9AA4 FB39 3AF2 73FF 56F9 \ 8251 C3A4 FE63
    297B 1CE1
  </vpk>

  <subsubsection|Binary signing key. <name|Philipp Hagemeister>
  (<long-id-spc|F5EA B582 FAFB 085C>)>

  <index-complex|<tuple|Keys|People|Hagemeister,
  Philipp|0xF5EAB582FAFB085C>|||<tuple|Keys|People|Hagemeister,
  Philipp|<verb-sm|0xF5EAB582FAFB085C>>>A binary signing key used by
  <subindex|People|Hagemeister, Philipp>Philipp Hagemeister to sign releases
  sometime before 2021.<\footnote>
    See <hlinkv|https://phihag.de/keys/A4826A18.asc>.
  </footnote>

  <\vpk>
    pub \ \ dsa1024/0xF5EAB582FAFB085C 2006-10-23 [SCA] [expired: 2015-12-31]

    \ \ \ \ \ \ Key fingerprint = 0600 E1DB 6FB5 3A5D 95D8 \ FC0D F5EA B582
    FAFB 085C

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Philipp Hagemeister
    \<less\>6a728fcb\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Philipp Hagemeister
    \<less\>9482dbb6\<gtr\>
  </vpk>

  <subsubsection|Binary signing key. <name|Philipp Hagemeister>
  (<long-id-spc|DB4B 54CB A482 6A18>)>

  <index-complex|<tuple|Keys|People|Hagemeister,
  Philipp|0xDB4B54CBA4826A18>|||<tuple|Keys|People|Hagemeister,
  Philipp|<verb-sm|0xDB4B54CBA4826A18>>>A binary signing key used used by
  <subindex|People|Hagemeister, Philipp>Philipp Hagemeister to sign releases
  sometime before 2021.

  <\vpk>
    pub \ \ rsa4096/0xDB4B54CBA4826A18 2013-01-11 [SC] [expires: 2033-01-06]

    \ \ \ \ \ \ Key fingerprint = 7D33 D762 FD6C 3513 0481 \ 347F DB4B 54CB
    A482 6A18

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Philipp Hagemeister
    \<less\>9482dbb6\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Philipp Hagemeister
    \<less\>3ec335f6\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Philipp Hagemeister
    \<less\>7787a9ff\<gtr\>

    sub \ \ rsa4096/0x862A257D825E38B8 2013-01-11 [E] [expires: 2033-01-06]

    \ \ \ \ \ \ Key fingerprint = 61F8 AC9E 8A81 6A5F 9BD8 \ B922 862A 257D
    825E 38B8
  </vpk>

  <subsubsection|Binary signing key. <name|Filippo Valsorda>
  (<long-id-spc|EBF0 1804 BCF0 5F6B>)>

  <index-complex|<tuple|Keys|People|Valsorda,
  Filippo|0xEBF01804BCF05F6B>|||<tuple|Keys|People|Valsorda,
  Filippo|<verb-sm|0xEBF01804BCF05F6B>>>A binary signing key used by
  <subindex|People|Valsorda, Filippo>Filippo Valsorda to sign releases
  sometime before 2021.

  <\vpk>
    pub \ \ rsa4096/0xEBF01804BCF05F6B 2012-08-30 [SCEA]

    \ \ \ \ \ \ Key fingerprint = 428D F5D6 3EF0 7494 BB45 \ 5AC0 EBF0 1804
    BCF0 5F6B

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Filippo Valsorda
    \<less\>7970bea1\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Filippo Valsorda
    \<less\>651b1dcc\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Filippo Valsorda
    \<less\>1b03dbe9\<gtr\>
  </vpk>

  <index-complex|<tuple|Software|Youtube-dl>|strong|c2 ytdl
  idx1|<tuple|Software|Youtube-dl>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|45>
    <associate|page-medium|papyrus>
    <associate|section-nr|18>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|19|45>>
    <associate|auto-10|<tuple|19.3.2|45>>
    <associate|auto-11|<tuple|<tuple|Keys|People|Hagemeister,
    Philipp|0xF5EAB582FAFB085C>|45>>
    <associate|auto-12|<tuple|People|45>>
    <associate|auto-13|<tuple|19.3.3|45>>
    <associate|auto-14|<tuple|<tuple|Keys|People|Hagemeister,
    Philipp|0xDB4B54CBA4826A18>|46>>
    <associate|auto-15|<tuple|People|46>>
    <associate|auto-16|<tuple|19.3.4|46>>
    <associate|auto-17|<tuple|<tuple|Keys|People|Valsorda,
    Filippo|0xEBF01804BCF05F6B>|46>>
    <associate|auto-18|<tuple|People|46>>
    <associate|auto-19|<tuple|<tuple|Software|Youtube-dl>|46>>
    <associate|auto-2|<tuple|19.1|45>>
    <associate|auto-3|<tuple|<tuple|Software|Youtube-dl>|45>>
    <associate|auto-4|<tuple|People|45>>
    <associate|auto-5|<tuple|19.2|45>>
    <associate|auto-6|<tuple|People|45>>
    <associate|auto-7|<tuple|19.3|45>>
    <associate|auto-8|<tuple|19.3.1|45>>
    <associate|auto-9|<tuple|<tuple|Keys|People|M.,
    Sergey|0x2C393E0F18A9236D>|45>>
    <associate|c2 ytdl|<tuple|19|45>>
    <associate|c2 ytdl-bg|<tuple|19.1|45>>
    <associate|c2 ytdl-hist|<tuple|19.2|45>>
    <associate|c2 ytdl-pk|<tuple|19.3|45>>
    <associate|footnote-19.1|<tuple|19.1|45>>
    <associate|footnote-19.10|<tuple|19.10|45>>
    <associate|footnote-19.2|<tuple|19.2|45>>
    <associate|footnote-19.3|<tuple|19.3|45>>
    <associate|footnote-19.4|<tuple|19.4|45>>
    <associate|footnote-19.5|<tuple|19.5|45>>
    <associate|footnote-19.6|<tuple|19.6|45>>
    <associate|footnote-19.7|<tuple|19.7|45>>
    <associate|footnote-19.8|<tuple|19.8|45>>
    <associate|footnote-19.9|<tuple|19.9|45>>
    <associate|footnr-19.1|<tuple|19.1|45>>
    <associate|footnr-19.10|<tuple|19.10|45>>
    <associate|footnr-19.2|<tuple|19.2|45>>
    <associate|footnr-19.3|<tuple|19.3|45>>
    <associate|footnr-19.4|<tuple|19.4|45>>
    <associate|footnr-19.5|<tuple|19.5|45>>
    <associate|footnr-19.6|<tuple|19.6|45>>
    <associate|footnr-19.7|<tuple|19.7|45>>
    <associate|footnr-19.8|<tuple|19.8|45>>
    <associate|footnr-19.9|<tuple|19.9|45>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Youtube-dl>|strong|c2 ytdl
      idx1|<tuple|Software|Youtube-dl>|<pageref|auto-3>>

      <tuple|<tuple|People|M., Sergey>|<pageref|auto-4>>

      <tuple|<tuple|People|Garcia, Ricardo>|<pageref|auto-6>>

      <tuple|<tuple|Keys|People|M., Sergey|0x2C393E0F18A9236D>|||<tuple|Keys|People|D.,
      Sergey|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x2C393E0F18A9236D>>>|<pageref|auto-9>>

      <tuple|<tuple|Keys|People|Hagemeister,
      Philipp|0xF5EAB582FAFB085C>|||<tuple|Keys|People|Hagemeister,
      Philipp|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xF5EAB582FAFB085C>>>|<pageref|auto-11>>

      <tuple|<tuple|People|Hagemeister, Philipp>|<pageref|auto-12>>

      <tuple|<tuple|Keys|People|Hagemeister,
      Philipp|0xDB4B54CBA4826A18>|||<tuple|Keys|People|Hagemeister,
      Philipp|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xDB4B54CBA4826A18>>>|<pageref|auto-14>>

      <tuple|<tuple|People|Hagemeister, Philipp>|<pageref|auto-15>>

      <tuple|<tuple|Keys|People|Valsorda,
      Filippo|0xEBF01804BCF05F6B>|||<tuple|Keys|People|Valsorda,
      Filippo|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xEBF01804BCF05F6B>>>|<pageref|auto-17>>

      <tuple|<tuple|People|Valsorda, Filippo>|<pageref|auto-18>>

      <tuple|<tuple|Software|Youtube-dl>|strong|c2 ytdl
      idx1|<tuple|Software|Youtube-dl>|<pageref|auto-19>>
    </associate>
    <\associate|toc>
      19<space|2spc><with|font-shape|<quote|small-caps>|Youtube-dl>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|19.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|19.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|19.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|2tab>|19.3.1<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Sergey M.>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|2C39
      3E0F 18A9 236D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|2tab>|19.3.2<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Philipp Hagemeister>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|F5EA
      B582 FAFB 085C>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|19.3.3<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Philipp Hagemeister>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DB4B
      54CB A482 6A18>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|19.3.4<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Filippo Valsorda>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|EBF0
      1804 BCF0 5F6B>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>
    </associate>
  </collection>
</auxiliary>