<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|Amazon Web Services><label|c2 aws>

  <name|Amazon Web Services> (AWS) is a cloud services company.

  <\remark>
    As of 2022-01-13, various PGP keys exist for various services such as
    CDK, JSII, ECS, SES, RFDK, and others. Lack of notable websites linking
    to these services, much less indicating the importance of such keys,
    means these public keys were not included. The lineage of security
    vulnerability public keys is included in this document since security
    vulnerabilities may reasonably be expected to exist so long as any AWS
    service is active.
  </remark>

  <subsection|Background><label|c2 aws-bg>

  \;

  <subsection|History><label|c2 aws-hist>

  <\description>
    <item*|2006-08-18>Early mention on the <name|Internet Archive> (IA) of
    AWS on <hlink|<verbatim|amazon.com>|https://amazon.com> . <\footnote>
      See <hlinkv|https://web.archive.org/web/20060818023744/http://www.amazon.com/b?ie=UTF8&node=3435361>.
    </footnote>

    <item*|2008-08-19>First IA snapshot of
    <hlink|<verbatim|aws.amazon.com>|https://aws.amazon.com> .<\footnote>
      See <hlinkv|https://web.archive.org/web/20080819152311/https://aws.amazon.com/>.
    </footnote>

    <item*|2010-07-20>Creation date of early AWS Security PGP key
    (<long-id-spc|56A1 1FDD 83B8 2FE5>).
  </description>

  \;

  <subsection|Public Key Details><label|c2 aws-pk>

  <subsubsection|AWS Security (2010-07-20/2011-07-20 ) (<long-id-spc|56A1
  1FDD 83B8 2FE5>)>

  A public key<\footnote>
    See <hlinkv|https://web.archive.org/web/20100727092952/http://aws.amazon.com/security/aws-pgp-public-key>.
  </footnote> advertised for use in reporting security
  vulnerabilities.<\footnote>
    See <hlinkv|https://web.archive.org/web/20100727044252/http://aws.amazon.com/security/vulnerability-reporting>.
  </footnote> Expired on 2011-07-20.

  <\vpk>
    pub \ \ rsa4096/0x56A11FDD83B82FE5 2010-07-20 [SC] [expired: 2011-07-20]

    \ \ \ \ \ \ Key fingerprint = 45E4 12AF 7C13 04DA 8FBE \ 5B81 56A1 1FDD
    83B8 2FE5

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] AWS Security Team
    \<less\>5a583163\<gtr\>
  </vpk>

  <subsubsection|AWS Security (2011-07-06/2014-07-20) (<long-id-spc|DC44 4A9A
  F27B 711F>)>

  A public key<\footnote>
    See <hlinkv|https://web.archive.org/web/20111223224241/http://aws.amazon.com/security/aws-pgp-public-key>.
  </footnote> advertised for use in reporting security vulnerabilities.
  Expired on 2014-07-20.

  <\vpk>
    pub \ \ rsa4096/0xDC444A9AF27B711F 2011-07-06 [SCEA] [expired:
    2014-07-20]

    \ \ \ \ \ \ Key fingerprint = 4AD3 6271 A9D4 BFC4 713B \ 61F7 DC44 4A9A
    F27B 711F

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] AWS Security
    \<less\>5a583163\<gtr\>
  </vpk>

  <subsubsection|AWS Security (2014-05-09/2017-05-09) (<long-id-spc|41CE 655D
  2795 3AB7>)>

  A public key<\footnote>
    See <hlinkv|https://web.archive.org/web/20140628022944/http://aws.amazon.com:80/security/aws-pgp-public-key/>.
  </footnote> advertised for use in reporting security vulnerabilities.
  Expired on 2017-05-09.

  <\vpk>
    pub \ \ rsa4096/0x41CE655D27953AB7 2014-05-09 [SCEA] [revoked:
    2017-06-28]

    \ \ \ \ \ \ Key fingerprint = A6F6 5096 D3C4 3873 A4EE \ 37FB 41CE 655D
    2795 3AB7

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ revoked] AWS Security
    \<less\>5a583163\<gtr\>
  </vpk>

  <subsubsection|AWS Security (2019-01-15/ - ) (<long-id-spc|12C6 4E7B 8490
  A11E>)>

  A public key<\footnote>
    See <hlinkv|https://aws.amazon.com/security/aws-pgp-public-key/>.
  </footnote> advertised for use in reporting security
  vulnerabilities<\footnote>
    See <hlinkv|https://aws.amazon.com/security/vulnerability-reporting/>.
  </footnote> as of 2022-01-13.

  <\vpk>
    pub \ \ rsa4096/0x12C64E7B8490A11E 2019-01-15 [SC] [expires: 2024-01-14]

    \ \ \ \ \ \ Key fingerprint = CA54 8A1A 5E71 7A9A CB10 \ C7AA 12C6 4E7B
    8490 A11E

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] AWS Security
    \<less\>5a583163\<gtr\>

    sub \ \ rsa4096/0x423ABDB7FF6F4FA0 2019-01-15 [E] [expires: 2024-01-14]

    \ \ \ \ \ \ Key fingerprint = 15D5 DECE CAE1 6D41 E1CA \ 12F5 423A BDB7
    FF6F 4FA0

    \;
  </vpk>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|9>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|9>>
    <associate|auto-2|<tuple|1.1|9>>
    <associate|auto-3|<tuple|1.2|9>>
    <associate|auto-4|<tuple|1.3|9>>
    <associate|auto-5|<tuple|1.3.1|9>>
    <associate|auto-6|<tuple|1.3.2|9>>
    <associate|auto-7|<tuple|1.3.3|9>>
    <associate|auto-8|<tuple|1.3.4|9>>
    <associate|c2 aws|<tuple|1|9>>
    <associate|c2 aws-bg|<tuple|1.1|9>>
    <associate|c2 aws-hist|<tuple|1.2|9>>
    <associate|c2 aws-pk|<tuple|1.3|9>>
    <associate|footnote-1.1|<tuple|1.1|9>>
    <associate|footnote-1.2|<tuple|1.2|9>>
    <associate|footnote-1.3|<tuple|1.3|9>>
    <associate|footnote-1.4|<tuple|1.4|9>>
    <associate|footnote-1.5|<tuple|1.5|9>>
    <associate|footnote-1.6|<tuple|1.6|9>>
    <associate|footnote-1.7|<tuple|1.7|9>>
    <associate|footnote-1.8|<tuple|1.8|9>>
    <associate|footnr-1.1|<tuple|1.1|9>>
    <associate|footnr-1.2|<tuple|1.2|9>>
    <associate|footnr-1.3|<tuple|1.3|9>>
    <associate|footnr-1.4|<tuple|1.4|9>>
    <associate|footnr-1.5|<tuple|1.5|9>>
    <associate|footnr-1.6|<tuple|1.6|9>>
    <associate|footnr-1.7|<tuple|1.7|9>>
    <associate|footnr-1.8|<tuple|1.8|9>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      1<space|2spc>Amazon Web Services <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|2tab>|1.3.1<space|2spc>AWS Security
      (2010-07-20/2011-07-20 ) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|2tab>|1.3.2<space|2spc>AWS Security
      (2011-07-06/2014-07-20) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|2tab>|1.3.3<space|2spc>AWS Security
      (2014-05-09/2017-05-09) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|2tab>|1.3.4<space|2spc>AWS Security (2019-01-15/
      - ) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>
    </associate>
  </collection>
</auxiliary>