<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Freedombox>><label|c2 freedombox>

  Last updated on 2023-06-12 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 freedombox-bg>

  <index-complex|<tuple|Software|Freedombox>|strong|c2 freedombox
  idx1|<tuple|Software|Freedombox>><name|FreedomBox<\footnote>
    Main website: <hlinkv|https://www.freedombox.org/>.
  </footnote>> is an operating system based on
  <subindex|Software|Debian><name|GNU/Linux Debian> designed to provide
  individuals locally-hosted internet services in lieu of popular cloud
  services such as those offered by <name|Google>, <name|Microsoft>,
  <name|Facebook>, and <name|Apple>. Services include:

  <\itemize>
    <item>Publishing and Blogging (via <name|Wordpress>, <name|ikiwiki>, and
    <name|MediaWiki>).

    <item>File sharing (via <name|Bepasty> and other apps).

    <item>Version control (via <name|GitWeb>).

    <item>End-to-end encrypted chat (via <name|Matrix>)
  </itemize>

  The operating system is designed to run on low power hardware such as
  single-board computers including the <index|Raspberry Pi><name|Raspberry
  Pi>.

  The software is maintained by the <subindex|Organizations|Freedombox
  Foundation><name|FreedomBox Foundation> which was founded in 2010 by
  <subindex|People|Moglen, Eben><name|Eben
  Moglen>.<cite|nyt_20110215_moglen-founds-fbx>

  <subsection|History><label|c2 freedombox-hist>

  <\description>
    <item*|2010-08-08>First I.A. snapshot of
    <hlinkv|http://wiki.debian.org/FreedomBox>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20100808091841/http://wiki.debian.org/FreedomBox>.
    </footnote>

    <item*|2011-02-17>First I.A. snapshot of
    <hlinkv|http://freedomboxfoundation.org>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20110217045826/http://freedomboxfoundation.org/>.
    </footnote>

    <item*|2011-02-15><name|New York Times> publishes article about creation
    of the <name|Freedom Box Foundation> by <name|Eben
    Moglen>.<cite|nyt_20110215_moglen-founds-fbx>

    <item*|2011-11-12>Creation date of signing key <long-id-spc|36C3 6144
    0C9B C971> (<name|Sunil Mohan Adapa>).

    <item*|2015-06-07>Creation date of signing key <long-id-spc|77C0 C75E
    7B65 0808> (<name|James Valleroy>).

    <item*|2018-06-06>Creation date of signing key <long-id-spc|5D41 53D6
    FE18 8FC8> (<name|FreedomBox> C.I. server).
  </description>

  <subsection|Public Key Details><label|c2 freedombox-pk>

  <\vgroup>
    <subsubsection|Signing key (2015\U2019) (<long-id-spc|36C3 6144 0C9B
    C971>)>

    <index-complex|<tuple|Keys|People|Adapa, Sunil
    Mohan|0x36C361440C9BC971>|||<tuple|Keys|People|Adapa, Sunil
    Mohan|<verb-sm|0x36C361440C9BC971>>>Key used by <name|Sunil Mohan Adapa>
    to sign <name|FreedomBox> releases between 2015 and 2019.<\footnote>
      See <hlinkv|https://web.archive.org/web/20150926202452/https://wiki.debian.org/FreedomBox/Download>.
    </footnote> <\footnote>
      <label|20220513-fbx-sig-dates>See <hlinkv|https://reboil.com/res/2022/txt/20220514T0328Z..fbx_img_sig_dates.html>.
    </footnote>

    <\vpk>
      pub \ \ rsa4096/0x36C361440C9BC971 2011-11-12 [SC] [expires:
      2024-04-01]

      \ \ \ \ \ \ Key fingerprint = BCBE BD57 A11F 70B2 3782 \ BC57 36C3 6144
      0C9B C971

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Sunil Mohan Adapa
      \<less\>0f990da0\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Sunil Mohan Adapa
      \<less\>040cbdcf\<gtr\>

      sub \ \ rsa4096/0x43EA1CFF0AA7C5F2 2016-06-04 [S] [expires: 2024-04-01]

      \ \ \ \ \ \ Key fingerprint = E713 C363 D672 5A75 AEA5 \ 7481 43EA 1CFF
      0AA7 C5F2

      sub \ \ rsa4096/0xF9F18B3DA6EF2942 2016-06-04 [A] [expires: 2024-04-01]

      \ \ \ \ \ \ Key fingerprint = DDE7 318C 9541 FB42 E786 \ 8DD0 F9F1 8B3D
      A6EF 2942

      sub \ \ rsa4096/0xF5077A854C1D4B57 2011-11-12 [E] [expires: 2024-04-01]

      \ \ \ \ \ \ Key fingerprint = 83BB 47F9 531E 732C D468 \ E382 F507 7A85
      4C1D 4B57
    </vpk>
  </vgroup>

  <\vgroup>
    <subsubsection|Signing key (2016\U2017) (<long-id-spc|77C0 C75E 7B65
    0808>)>

    <index-complex|<tuple|Keys|People|Valleroy,
    James|0x77C0C75E7B650808>|||<tuple|Keys|People|Valleroy,
    James|<verb-sm|0x77C0C75E7B650808>>>Key used by James Valleroy to sign
    <name|FreedomBox> releases in between 2016 and
    2017<rsup|<reference|20220513-fbx-sig-dates>>.

    <\vpk>
      \;

      pub \ \ rsa4096/0x77C0C75E7B650808 2015-06-07 [SCA] [expires:
      2023-11-19]

      \ \ \ \ \ \ Key fingerprint = 7D6A DB75 0F91 0855 8948 \ 4BE6 77C0 C75E
      7B65 0808

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] James Valleroy
      \<less\>ef8c790c\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] James Valleroy
      \<less\>cc9e6d5c\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] James Valleroy
      \<less\>aed00202\<gtr\>

      sub \ \ rsa4096/0x1E3D7658DDA11207 2015-07-03 [A] [expires: 2023-11-19]

      \ \ \ \ \ \ Key fingerprint = 065E 1FA3 78BD C472 41A9 \ 410F 1E3D 7658
      DDA1 1207

      sub \ \ rsa2048/0x81DD8ABA2A624357 2015-12-22 [A] [expires: 2023-11-19]

      \ \ \ \ \ \ Key fingerprint = 14ED DCE5 A2C1 5C06 E474 \ 0AC0 81DD 8ABA
      2A62 4357

      sub \ \ rsa4096/0x4972352E25D22BF4 2015-06-07 [E] [expires: 2023-11-19]

      \ \ \ \ \ \ Key fingerprint = 1968 4269 1A27 66F2 1A8B \ 250C 4972 352E
      25D2 2BF4

      sub \ \ rsa2048/0xBEE22CB4990F243B 2017-01-14 [A] [expires: 2023-11-19]

      \ \ \ \ \ \ Key fingerprint = BCCD 6658 3F68 0306 8299 \ 79AE BEE2 2CB4
      990F 243B
    </vpk>
  </vgroup>

  <\vgroup>
    <subsubsection|Signing key (2018\U2022) (<long-id-spc|5D41 53D6 FE18
    8FC8>)>

    <index-complex|<tuple|Keys|People|Valleroy,
    James|0x77C0C75E7B650808>|||<tuple|Keys|People|Valleroy,
    James|<verb-sm|0x77C0C75E7B650808>>>Key used by the <name|FreedomBox>
    Continuous Integration server to sign <name|FreedomBox> releases from
    2018 through 2022<rsup|<reference|20220513-fbx-sig-dates>>.

    <\vpk>
      \;

      pub \ \ rsa4096/0x5D4153D6FE188FC8 2018-06-06 [SC]

      \ \ \ \ \ \ Key fingerprint = 013D 86D8 BA32 EAB4 A669 \ 1BF8 5D41 53D6
      FE18 8FC8

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] FreedomBox CI
      (Continuous Integration server) \<less\>7c8f3186\<gtr\>

      sub \ \ rsa4096/0x24E6B04F3B3AF25D 2018-06-06 [E]

      \ \ \ \ \ \ Key fingerprint = CE3D 01C4 C6D1 FF10 C201 \ 348D 24E6 B04F
      3B3A F25D
    </vpk>
  </vgroup>

  <index-complex|<tuple|Software|Freedombox>|strong|c2 freedombox
  idx1|<tuple|Software|Freedombox>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|26>
    <associate|page-medium|paper>
    <associate|section-nr|5>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|20220513-fbx-sig-dates|<tuple|6.5|26>>
    <associate|auto-1|<tuple|6|26>>
    <associate|auto-10|<tuple|6.3.1|26>>
    <associate|auto-11|<tuple|<tuple|Keys|People|Adapa, Sunil
    Mohan|0x36C361440C9BC971>|26>>
    <associate|auto-12|<tuple|6.3.2|27>>
    <associate|auto-13|<tuple|<tuple|Keys|People|Valleroy,
    James|0x77C0C75E7B650808>|27>>
    <associate|auto-14|<tuple|6.3.3|27>>
    <associate|auto-15|<tuple|<tuple|Keys|People|Valleroy,
    James|0x77C0C75E7B650808>|27>>
    <associate|auto-16|<tuple|<tuple|Software|Freedombox>|27>>
    <associate|auto-2|<tuple|6.1|26>>
    <associate|auto-3|<tuple|<tuple|Software|Freedombox>|26>>
    <associate|auto-4|<tuple|Software|26>>
    <associate|auto-5|<tuple|Raspberry Pi|26>>
    <associate|auto-6|<tuple|Organizations|26>>
    <associate|auto-7|<tuple|People|26>>
    <associate|auto-8|<tuple|6.2|26>>
    <associate|auto-9|<tuple|6.3|26>>
    <associate|c2 freedombox|<tuple|6|26>>
    <associate|c2 freedombox-bg|<tuple|6.1|26>>
    <associate|c2 freedombox-hist|<tuple|6.2|26>>
    <associate|c2 freedombox-pk|<tuple|6.3|26>>
    <associate|footnote-6.1|<tuple|6.1|26>>
    <associate|footnote-6.2|<tuple|6.2|26>>
    <associate|footnote-6.3|<tuple|6.3|26>>
    <associate|footnote-6.4|<tuple|6.4|26>>
    <associate|footnote-6.5|<tuple|6.5|26>>
    <associate|footnr-6.1|<tuple|6.1|26>>
    <associate|footnr-6.2|<tuple|6.2|26>>
    <associate|footnr-6.3|<tuple|6.3|26>>
    <associate|footnr-6.4|<tuple|6.4|26>>
    <associate|footnr-6.5|<tuple|6.5|26>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      nyt_20110215_moglen-founds-fbx

      nyt_20110215_moglen-founds-fbx
    </associate>
    <\associate|idx>
      <tuple|<tuple|Software|Freedombox>|strong|c2 freedombox
      idx1|<tuple|Software|Freedombox>|<pageref|auto-3>>

      <tuple|<tuple|Software|Debian>|<pageref|auto-4>>

      <tuple|<tuple|Raspberry Pi>|<pageref|auto-5>>

      <tuple|<tuple|Organizations|Freedombox Foundation>|<pageref|auto-6>>

      <tuple|<tuple|People|Moglen, Eben>|<pageref|auto-7>>

      <tuple|<tuple|Keys|People|Adapa, Sunil
      Mohan|0x36C361440C9BC971>|||<tuple|Keys|People|Adapa, Sunil
      Mohan|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x36C361440C9BC971>>>|<pageref|auto-11>>

      <tuple|<tuple|Keys|People|Valleroy,
      James|0x77C0C75E7B650808>|||<tuple|Keys|People|Valleroy,
      James|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x77C0C75E7B650808>>>|<pageref|auto-13>>

      <tuple|<tuple|Keys|People|Valleroy,
      James|0x77C0C75E7B650808>|||<tuple|Keys|People|Valleroy,
      James|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x77C0C75E7B650808>>>|<pageref|auto-15>>

      <tuple|<tuple|Software|Freedombox>|strong|c2 freedombox
      idx1|<tuple|Software|Freedombox>|<pageref|auto-16>>
    </associate>
    <\associate|toc>
      6<space|2spc><with|font-shape|<quote|small-caps>|Freedombox>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|6.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|6.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|6.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|6.3.1<space|2spc>Signing key (2015\U2019)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|36C3
      6144 0C9B C971>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|6.3.2<space|2spc>Signing key (2016\U2017)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|77C0
      C75E 7B65 0808>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|2tab>|6.3.3<space|2spc>Signing key (2018\U2022)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|5D41
      53D6 FE18 8FC8>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>>
    </associate>
  </collection>
</auxiliary>