<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|App Name>><label|c2 app>

  Last updated on 1970-01-01 by <person|Horace Worblehat>.

  <subsection|Background><label|c2 app-bg>

  <inactive|<index-complex|<tuple|Software|App Name>|strong|c2 app
  idx1|<tuple|Software|App Name>>><name|App Name<\footnote>
    Main website: <hlinkv|https://example.com>.
  </footnote>> is a program made by <inactive|<subindex|People|Stibbons,
  Ponder>>Ponder Stibbons (<long-id-spc|DEAD BEEF DEAD BEEF>).

  <subsection|History><label|c2 app-hist>

  <\description>
    <item*|1970-01-01>In the beginning<text-dots>
  </description>

  <subsection|Public Key Details><label|c2 app-pk>

  <subsubsection|Signing key (2021) (<long-id-spc|DEAD BEEF DEAD BEEF>)>

  <inactive|<index-complex|<tuple|Keys|People|Stibbons,
  Ponder|0xDEADBEEFDEADBEEF>|||<tuple|Keys|People|Stibbons,
  Ponder|<verb-sm|0xDEADBEEFDEADBEEF>>>>Key used by Ponder Stibbons to sign
  releases in 2021.

  <\vpk>
    foo bar
  </vpk>

  <inactive|<index-complex|<tuple|Software|App Name>|strong|c2 app
  idx1|<tuple|Software|App Name>>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|9>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|9>>
    <associate|auto-2|<tuple|1.1|9>>
    <associate|auto-3|<tuple|1.2|9>>
    <associate|auto-4|<tuple|1.3|9>>
    <associate|auto-5|<tuple|1.3.1|9>>
    <associate|c2 app|<tuple|1|9>>
    <associate|c2 app-bg|<tuple|1.1|9>>
    <associate|c2 app-hist|<tuple|1.2|9>>
    <associate|c2 app-pk|<tuple|1.3|9>>
    <associate|footnote-1.1|<tuple|1.1|9>>
    <associate|footnr-1.1|<tuple|1.1|9>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      1<space|2spc><with|font-shape|<quote|small-caps>|App Name>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|2tab>|1.3.1<space|2spc>Signing key (2021)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DEAD
      BEEF DEAD BEEF>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>
    </associate>
  </collection>
</auxiliary>