<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Yt-dlp>><label|c2 ytdlp>

  Last updated on 2023-06-24 by <person|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 ytdlp-bg>

  <index-complex|<tuple|Software|Yt-dlp>|strong|c2 ytdlp
  idx1|<tuple|Software|Yt-dlp>><name|Yt-dlp<\footnote>
    Main website: <hlinkv|https://github.com/yt-dlp/yt-dlp>.
  </footnote>> is a <name|Python3> fork of <name|Youtube-dl>. As of 2023, the
  fork is maintained by <subindex|People|pukkandan><person|pukkandan>
  (<long-id-spc|7EEE 9E1E 817D 0A39>)<\footnote>
    See <hlinkv|https://github.com/yt-dlp/yt-dlp/blob/de4cf77ec1a13f020e6afe4ed04248c6b19fccb6/CONTRIBUTORS>.
  </footnote> and signed by <person|Simon Sawicki> (<long-id-spc|57CF 6593
  3B5A 7581>).<\footnote>
    See <hlinkv|https://github.com/yt-dlp/yt-dlp/releases/tag/2023.06.22>.
  </footnote>

  Binaries are released from the <name|GitHub> repository page.<\footnote>
    See <hlinkv|https://github.com/yt-dlp/yt-dlp/releases>.
  </footnote> As of 2023-06-24, the latest version is <em|2023.06.22>.

  <subsection|History><label|c2 ytdlp-hist>

  <\description>
    <item*|2023-02-26>Creation date of <person|Simon Sawicki>'s
    <long-id-spc|57CF 6593 3B5A 7581> signing key.
  </description>

  <subsection|Public Key Details><label|c2 ytdlp-pk>

  <subsubsection|Signing key (2023\U ) (<long-id-spc|57CF 6593 3B5A 7581>)>

  <index-complex|<tuple|Keys|People|Sawicki,
  Simon|0x57CF65933B5A7581>|||<tuple|Keys|People|Sawicki,
  Simon|<verb-sm|0x57CF65933B5A7581>>>Key used by Simon Sawicki to sign
  releases since around 2023-03-03.

  <\vpk>
    pub \ \ rsa4096/0x57CF65933B5A7581 2023-02-26 [SC]

    \ \ \ \ \ \ Key fingerprint = AC0C BBE6 848D 6A87 3464 \ AF4E 57CF 6593
    3B5A 7581

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Simon Sawicki (yt-dlp
    signing key) \<less\>322ad34c\<gtr\>
  </vpk>

  <subsubsection|Commit signing key (2023\U ) (<long-id-spc|7EEE 9E1E 817D
  0A39>)>

  <index-complex|<tuple|Keys|People|pukkandan|0x7EEE9E1E817D0A39>|||<tuple|Keys|People|pukkandan|<verb-sm|0x7EEE9E1E817D0A39>>>Key
  used by the owner and maintainer of the <name|yt-dlp> <name|GitHub>
  repository, <person|pukkandan>, to sign commits since at least 2023.

  <\vpk>
    pub \ \ rsa4096/0x7EEE9E1E817D0A39 2022-02-10 [SC]

    \ \ \ \ \ \ Key fingerprint = 11A7 8F5F 3008 5974 54DF \ E412 7EEE 9E1E
    817D 0A39

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] pukkandan
    \<less\>ca75f104\<gtr\>

    sub \ \ rsa4096/0x9B8EB5BD8D3AA7C1 2022-02-10 [E]

    \ \ \ \ \ \ Key fingerprint = 58FD 15D6 9815 CF6F 7C2A \ 9875 9B8E B5BD
    8D3A A7C1
  </vpk>

  <index-complex|<tuple|Software|Yt-dlp>|strong|c2 ytdlp
  idx1|<tuple|Software|Yt-dlp>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|9>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|9>>
    <associate|auto-10|<tuple|<tuple|Keys|People|pukkandan|0x7EEE9E1E817D0A39>|?>>
    <associate|auto-11|<tuple|<tuple|Software|Yt-dlp>|?>>
    <associate|auto-2|<tuple|1.1|9>>
    <associate|auto-3|<tuple|<tuple|Software|Yt-dlp>|9>>
    <associate|auto-4|<tuple|People|9>>
    <associate|auto-5|<tuple|1.2|9>>
    <associate|auto-6|<tuple|1.3|?>>
    <associate|auto-7|<tuple|1.3.1|?>>
    <associate|auto-8|<tuple|<tuple|Keys|People|Sawicki,
    Simon|0x57CF65933B5A7581>|?>>
    <associate|auto-9|<tuple|1.3.2|?>>
    <associate|c2 ytdlp|<tuple|1|9>>
    <associate|c2 ytdlp-bg|<tuple|1.1|9>>
    <associate|c2 ytdlp-hist|<tuple|1.2|9>>
    <associate|c2 ytdlp-pk|<tuple|1.3|9>>
    <associate|footnote-1.1|<tuple|1.1|9>>
    <associate|footnote-1.2|<tuple|1.2|?>>
    <associate|footnote-1.3|<tuple|1.3|?>>
    <associate|footnote-1.4|<tuple|1.4|?>>
    <associate|footnr-1.1|<tuple|1.1|9>>
    <associate|footnr-1.2|<tuple|1.2|?>>
    <associate|footnr-1.3|<tuple|1.3|?>>
    <associate|footnr-1.4|<tuple|1.4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      1<space|2spc><with|font-shape|<quote|small-caps>|Yt-dlp>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|2tab>|1.3.1<space|2spc>Signing key (2021)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DEAD
      BEEF DEAD BEEF>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>
    </associate>
  </collection>
</auxiliary>