<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|List of Public Keys><label|c2 list-pubkeys>

  Each section in this chapter contains a story about a person or
  organization that uses a public-private key pair. Each story consists of
  some brief background information, a history of notable events, and public
  key information. Public keys are usually identified through key
  fingerprints. Links to public keys are made available where
  possible<\footnote>
    A set of minimal copies of <name|GnuPG >public keys is available in the
    <name|Git> repository of this book in <verbatim|ref/pgp_keys/>. File
    names contain the full 160-bit hexadecimal fingerprint.
  </footnote>.
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|9>
    <associate|page-medium|paper>
    <associate|preamble|false>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|2|9>>
    <associate|c2 list-pubkeys|<tuple|2|9>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnr-1|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>List
      of Public Keys> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>