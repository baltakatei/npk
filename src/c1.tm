<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Trust through Stories><label|c1 trust-through-stories>

  <section|Summary>

  This book contains stories about where certain public keys came from and a
  little about the people who use them.

  Some people use public key cryptography to digitally sign their works. They
  do this so others can prove where copies of such works came from. Usually,
  digital tools automatically verify these digital signatures so people don't
  have to manually. However, in order to verify such tools, at some point a
  person must verify at least one digital signature for themselves.

  <section|Background>

  As of 2022, most people, if they worry at all about where they download
  their software from, usually only check that there is a padlock symbol next
  to the URL in their browser. Thanks to the efforts of <name|Let's Encrypt>
  and other companies promoting use of digital signature technology known as
  TLS (a.k.a. SSL, HTTPS), most people can rely on that padlock symbol,
  provided they pay attention to the base domain of the URL (i.e. the
  \P<verbatim|<strong|google.com>>\Q of \P<verbatim|https://mail.<strong|google.com>>\Q).

  TLS works by having a user's web browser come installed with a set of
  public keys whose private keys are kept secure by IT professionals trusted
  by governments. These IT people are known as \Pcertificate authorities\Q
  (CA). Whenever a webmaster wants to authenticate themselves to visitors to
  their website, the webmaster may create their own public-private keypair
  and ask a CA to digitally sign their public key. Then, whenever a visitor's
  web browser downloads a webpage, the server uses the webmaster's private
  key to digitally sign the webpage. The web browser can then download the
  server's public key, see that it is signed by a CA whose public key it
  already knows about and trusts. This is the cryptographically-secured
  process that occurs whenever a web browser's padlock symbol indicates a
  secure TLS connection.

  However, for paranoid technically-minded people who want to take
  precautions against servers being hacked, CA private keys being
  compromised, or some form of man-in-the-middle attack, sometimes software
  developers use their own digital certificate systems to authenticate
  themselves. One such system is <name|OpenPGP>. Instead of relying upon CAs
  trusted by governments, each software developer is their own CA. Unlike
  with TLS and web browsers, users who wish to verify digital signatures on
  programs made by such developers must have some trusted means of
  identifying and acquiring the developers' public keys. With <name|OpenPGP>,
  although it is possible in theory to create and maintain a \PWeb of Trust\Q
  by having key owners regularly sign eachothers' keys based upon their
  personal relationships with one another, in practice this method of
  establishing trust is outcompeted by the simplicity of using TLS; if the
  stakes of misidentifying a team member on a project are high enough, it is
  much simpler to simply meet in-person.

  <section|Purpose>

  That said, the purpose of this book is to provide you, reader, a means of
  identifying public keys used to sign notable software and data. Notability
  is defined and applied as in <name|Wikipedia>: it is a test to determine
  whether a chapter about an entity's public keys should be included. Where
  potential for confusion exists around the identity of a notable entity that
  maintains a public key, this book should identify that key.

  This document is a tertiary reference meant to paint a narrative about how
  and by whom a public key is used. Often public keys are secured by
  individual software developers and used to sign commits made in their
  version control systems. Some public keys are used by an individual but to
  represent an entire company or project. Although most public keys in this
  book are <name|OpenPGP> keys compatible with the <name|GnuPG> program, some
  public keys may use other systems or protocols such as those in TLS
  certificates, SSH key pairs, or cryptocurrency wallets<\footnote>
    E.g.: The address of the first spendable Bitcoin. See
    <hlinkv|https://chainflyer.bitflyer.com/Block/Height/1>.
  </footnote>, as long as they are notable.

  This book started as a set of personal notes I began maintaining in 2018 to
  help me verify software packages that I use. In 2021 I decided to share
  these notes in book-form with the help of the <name|GNU> <TeXmacs>
  typesetting program (mainly for its indexing and open-source nature). As of
  2022, the method of verification of key notability (me, <long-id-spc|A0A2
  95AB DC34 69C9>, scanning the web for fingerprints and keys of programs I
  use) is not scalable. However, this book uses the <name|Git> version
  control system and lives in a <name|GitLab> repository so additional
  collaborators (you) could help this book grow.

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|0>
    <associate|page-first|7>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|7>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-3|<tuple|1.2|?>>
    <associate|auto-4|<tuple|1.3|?>>
    <associate|c1 trust-through-stories|<tuple|1|7>>
    <associate|footnote-1.3.1|<tuple|1.3.1|?>>
    <associate|footnote-1.3.2|<tuple|1.3.2|?>>
    <associate|footnr-1.3.1|<tuple|1.3.1|?>>
    <associate|footnr-1.3.2|<tuple|1.3.2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Trust
      through Stories> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>