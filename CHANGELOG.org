* Change Log
** Changes in 0.4.0 (2023-06-24)
- Add stories for Element, Inkscape, Trisquel, Yt-dlp.
- Reviewed and updated stories for Bitcoin Core, Debian, Electrum,
  FreedomBox, KeePassXC, QubesOS, Satoshi Labs, Veracrypt, Yt-dlp,
  Youtube-dl.
